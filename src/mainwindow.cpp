/* mainwindow.cpp -- implementation for MainWindow
 * Copyright (C) 2015 Kaleb Klein
 * For license info, refer to updater.pro
 */

/* @(#) $Id$ */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->bYes, SIGNAL(clicked(bool)), this, SLOT(onYesClick()));
    connect(ui->bNo, SIGNAL(clicked(bool)), this, SLOT(onNoClick()));

    ui->textBrowser->setHtml(QString("<!DOCTYPE html>\
                                     <html lang=\"en\">\
                                     <head>\
                                         <meta charset=\"UTF-8\">\
                                         <title>Recent Changes</title>\
                                         <style type=\"text/css\">\
                                             body, html\
                                             {\
                                                 /*background-color: white;*/\
                                             }\
                                         </style>\
                                     </head>\
                                     <body>\
                                         <p>This is the page that will show the recent changes to KSE and any of it's modules</p>\
                                         <p>This window parses HTML and CSS, so a sexy changes page will be put here soon ;D</p>\
                                     </body>\
                                     </html>"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setVersionInfo(QString version, int versionCode, QString dlUrl)
{
    this->version = version;
    this->versionCode = versionCode;
    this->dlUrl = dlUrl;
}

void MainWindow::onYesClick()
{
    QMessageBox::information(this, "Updater", "Thanks for trying! The downloader is not implemented yet. But so far so good :D Click \"No\" to close the window.");
}

void MainWindow::onNoClick()
{
    qApp->quit();
}
