#include "mainclass.h"

MainClass::MainClass(QObject *parent) : QObject(parent)
{

}

void MainClass::checkForUpdate(int versionCode)
{
    checker = new UpdateCheck(this);
    connect(checker, SIGNAL(checkComplete(bool, QString)), this, SLOT(onCompleted(bool, QString)));

    checker->setTitle("Check for Updates");
    checker->setLabelText("Checking for updates, please wait...");
    checker->setMarqueBar(true);
    checker->setUrl("http://cdn.kalebklein.com/kseupdater/update.json");
    checker->check(versionCode);
}

void MainClass::onCompleted(bool newUpdate, QString dlUrl)
{
    checker->close();
    this->newUpdate = newUpdate;
    this->dlUrl = dlUrl;
}

bool MainClass::isUpdateAvailable()
{
    return this->newUpdate;
}

QString MainClass::getDownloadUrl()
{
    return this->dlUrl;
}
