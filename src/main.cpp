/* main.cpp -- Entry point for application
 * Copyright (C) 2015 Kaleb Klein
 * For license info, refer to updater.pro
 */

/* @(#) $Id$ */

#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>
#include <QFile>

#include "mainclass.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    int versionCode;
    QString version;
    MainClass m;

    QFile f(":styles/qdarkstyle.css");
    if(f.exists())
    {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
    }

    if(argc >= 1)
    {
        if((QString)argv[0] != "version")
        {
            if((QString)argv[1] == "version" && (QString)argv[3] == "versionCode")
            {
                versionCode = atoi(argv[4]);
                version = argv[2];
                m.checkForUpdate(versionCode);
                if(m.isUpdateAvailable())
                {
                    w.setVersionInfo(version, versionCode, m.getDownloadUrl());
                }
                else
                    return 0;
            }
            else
            {
                QMessageBox::critical(0, "Error", "The required information was not found. Did you attempt to run outside of KSE?");
                return 0;
            }
        }
        else if((QString)argv[0] == "version")
        {
            if((QString)argv[0] == "version" && (QString)argv[2] == "versionCode")
            {
                versionCode = atoi(argv[3]);
                version = argv[1];
                m.checkForUpdate(versionCode);
                if(m.isUpdateAvailable())
                {
                    w.setVersionInfo(version, versionCode, m.getDownloadUrl());
                }
                else
                    return 0;
            }
            else
            {
                QMessageBox::critical(0, "Error", "The required information was not found. Did you attempt to run outside of KSE?");
                return 0;
            }
        }
        else
        {
            QMessageBox::critical(0, "Error", "Incorrect information was passed to KSE updater. Please check the information and try again.");
        }
    }
    else
    {
        QMessageBox::critical(0, "Error", "The required information was not found. Did you attempt to run outside of KSE?");
        return 0;
    }

    w.show();
    return a.exec();
}
