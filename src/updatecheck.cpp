/* updatecheck.cpp -- implementation for update checker
 * Copyright (C) 2015 Kaleb Klein
 * For license info, refer to updater.pro
 */

/* @(#) $Id$ */

#include "updatecheck.h"

UpdateCheck::UpdateCheck(QObject *parent) : QObject(parent)
{
    dialog = new QProgressDialog();
    connect(dialog, SIGNAL(canceled()), this, SLOT(canceledCheck()));
    autoClose = false;
}

void UpdateCheck::setTitle(QString title)
{
    dialog->setWindowTitle(title);
}

void UpdateCheck::setLabelText(QString labelText)
{
    dialog->setLabelText(labelText);
}

void UpdateCheck::setMarqueBar(bool marque)
{
    if(marque)
        dialog->setMaximum(0);
}

void UpdateCheck::setUrl(QString url)
{
    this->url = QUrl(url);
}

void UpdateCheck::close()
{
    autoClose = true;
    dialog->close();
}

void UpdateCheck::canceledCheck()
{
    if(!autoClose)
        QMessageBox::information(0, "Update Canceled", "You have canceled the update check");
}

void UpdateCheck::check(int versionCode)
{
    dialog->show();

    QEventLoop evt;

    connect(&mgr, SIGNAL(finished(QNetworkReply*)), &evt, SLOT(quit()));

    QNetworkRequest req(this->url);
    reply = mgr.get(req);
    evt.exec();

    if(reply->error() == QNetworkReply::NoError)
    {
        QString str = (QString)reply->readAll();

        QJsonDocument jDoc = QJsonDocument::fromJson(str.toUtf8());
        QJsonObject json = jDoc.object();

        QJsonObject versionInfo = json["versionInfo"].toObject();

        int olv = versionInfo["versionCode"].toInt();
        if(olv > versionCode)
            emit checkComplete(true, json["updateArchive"].toString());
        else
            emit checkComplete(false, "");

        delete reply;
    }
    else
    {
        QMessageBox::critical(0, "Update Error", reply->errorString());
        delete reply;
        dialog->close();
    }
}
