/* mainwindow.h -- header for the main window C script
 * Copyright (C) 2015 Kaleb Klein
 * For license info, refer to updater.pro
 */

/* @(#) $Id$ */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

// Networking
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setVersionInfo(QString version, int versionCode, QString dlUrl);

private:
    Ui::MainWindow *ui;
    QString version;
    int versionCode;
    QString dlUrl;
    QNetworkAccessManager mgr;

public slots:
    void onYesClick();
    void onNoClick();

};

#endif // MAINWINDOW_H
