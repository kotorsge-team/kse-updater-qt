/* version.h -- Definition file for resource.rc
 * Copyright (C) 2015 Kaleb Klein
 * For license info, refer to updater.pro
 */

/* @(#) $Id$ */

#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             1,0,0,0
#define VER_FILEVERSION_STR         "1.0\0"

#define VER_PRODUCTVERSION          1,0,0,0
#define VER_PRODUCTVERSION_STR      "1.0\0"

#define VER_COMPANYNAME_STR         "KSE Team"
#define VER_FILEDESCRIPTION_STR     "KSE Updater"
#define VER_INTERNALNAME_STR        "Updater"
#define VER_LEGALCOPYRIGHT_STR      "Copyright (c) 2015 KSE Team"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "kseupd.exe"
#define VER_PRODUCTNAME_STR         "Updater"

#define VER_COMPANYDOMAIN_STR       "kalebklein.com"

#endif // VERSION_H
