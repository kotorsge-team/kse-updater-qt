/* updatecheck.h -- header for the updatecheck C script
 * Copyright (C) 2015 Kaleb Klein
 * For license info, refer to updater.pro
 */

/* @(#) $Id$ */

#ifndef UPDATECHECK_H
#define UPDATECHECK_H

#include <QApplication> // for QEventLoop

#include <QObject>
#include <QProgressDialog>
#include <QMessageBox>

// Networking
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

// JSON Data
#include <QJsonDocument>
#include <QJsonObject>

class UpdateCheck : public QObject
{
    Q_OBJECT
public:
    explicit UpdateCheck(QObject *parent = 0);
    void setTitle(QString title = "Dialog");
    void setLabelText(QString labelText);
    void setMarqueBar(bool marque = false);
    void setUrl(QString url);
    void check(int versionCode);
    void close();

private:
    QUrl url;
    QNetworkAccessManager mgr;
    QNetworkReply *reply;
    QProgressDialog *dialog;
    bool autoClose;

signals:
    void checkComplete(bool newUpdate, QString dlUrl);

public slots:
    void canceledCheck();

};

#endif // UPDATECHECK_H
