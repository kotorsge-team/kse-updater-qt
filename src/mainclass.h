#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QObject>
#include <QMessageBox>
#include "updatecheck.h"

class MainClass : public QObject
{
    Q_OBJECT
public:
    explicit MainClass(QObject *parent = 0);
    void checkForUpdate(int versionCode);
    bool isUpdateAvailable();
    QString getDownloadUrl();

private:
    UpdateCheck *checker;
    bool newUpdate;
    QString dlUrl;

public slots:
    void onCompleted(bool newUpdate, QString dlUrl);
};

#endif // MAINCLASS_H
