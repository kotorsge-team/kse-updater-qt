# updater.pro -- Project spec file for Updater Qt project
# Copyright (C) 2015 KSE Team

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

QT       += core gui network

# The following 2 options define the locations for gcc and g++
# Change or remove depending on your machine setup
# Also, comment out when debugging in Qt Creator. Leaving them causes
# The application to fail when attempting to build
#QMAKE_CXX=C:\MinGW\bin\g++.exe
#QMAKE_CC=C:\MinGW\bin\gcc.exe

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = updater
TEMPLATE = app

RC_FILE += src/resource.rc

SOURCES += src/main.cpp\
    src/updatecheck.cpp \
    src/mainclass.cpp \
    src/mainwindow.cpp

HEADERS  += \
    src/version.h \
    src/updatecheck.h \
    src/mainclass.h \
    src/mainwindow.h

FORMS    += \
    src/mainwindow.ui

RESOURCES += \
    src/qt_resource.qrc
