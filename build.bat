@echo off
color 0A
title Building KSE Updater
set QT=C:\Qt\5.4\mingw491_32\bin\
set MINGW=C:\MinGW\bin\
set QMAKE=%QT%qmake.exe
set MAKE=%MINGW%mingw32-make.exe
set CWD=%~dp0
rem Set's temporary path so g++ can read required dlls from mingw
set PATH=%QT%;%MINGW%;%PATH%
rem change the number depending on the number of cores on your CPU
set THREADS=-j8
if exist src\Makefile (
	cd src
	echo Cleaning Makefile
	if exist Makefile del Makefile
	echo Cleaning Makefile.Debug
	if exist Makefile.Debug del Makefile.Debug
	echo Cleaning Makefile.Release
	if exist Makefile.Release del Makefile.Release
	echo Cleaning debug directory
	if exist debug rmdir /S /Q debug
	echo Cleaning release directory
	if exist release rmdir /S /Q release
echo done!
) else (
	"%QMAKE%" -makefile -o src\Makefile updater.pro
	cd src
	"%MAKE%" %THREADS%
)
pause
